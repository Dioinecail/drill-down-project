﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBehaviour : PoolableBase
{
    // parameters
    public float speed;
    public float damage;
    public float collisionRadius;
    public LayerMask collisionMask;

    // cache
    private Vector2 direction;
    private Collider2D[] collisionBuffer = new Collider2D[1];



    public void SetDirection(Vector3 direction)
    {
        SetDirection((Vector2)direction);
    }

    public void SetDirection(Vector2 direction)
    {
        this.direction = direction;
    }

	void Update ()
    {
        transform.position += (Vector3)(direction * speed * Time.deltaTime);

        collisionBuffer[0] = null;

        Physics2D.OverlapCircleNonAlloc((Vector2)transform.position, collisionRadius, collisionBuffer, collisionMask);

        if(collisionBuffer[0] != null)
        {
            DamageableBase damagable = collisionBuffer[0].GetComponent<DamageableBase>();

            if (damagable != null)
            {
                damagable.ReceiveDamage(damage);
                ReturnToPool();
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, collisionRadius);
    }
}