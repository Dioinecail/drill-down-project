﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJumpBehaviour : AbstractBehaviour
{
    public float JumpSpeed = 5.5f;

    private bool canDoubleJump;



    private void Update()
    {
        if (Input.GetButtonDown("Jump") && !collisionBehaviour.onGround)
        {
            if(canDoubleJump)
                OnJump();
        }

        Anima.SetFloat("VelocityY", rBody.velocity.y);
    }

    public void OnJump()
    {
        Anima.Play("Jump");
        rBody.isKinematic = false;
        rBody.velocity = new Vector3(rBody.velocity.x, JumpSpeed);
        canDoubleJump = false;
    }

    private void AllowJump()
    {
        canDoubleJump = true;
    }

    private void OnEnable()
    {
        JumpBehaviour.onJump += AllowJump;
    }

    private void OnDisable()
    {
        JumpBehaviour.onJump -= AllowJump;
    }
}
