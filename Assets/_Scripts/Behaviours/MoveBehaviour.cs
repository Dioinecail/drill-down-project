﻿using UnityEngine;

public class MoveBehaviour : AbstractBehaviour
{
    // referenses
    public Transform player;

    // parameters
    public float moveSpeed = 5;
    public float accelerationSpeed = 5;

    // cache
    private new Transform transform;
    private float previousVelocity = 0;
    private float moveDirection;



    protected override void Awake()
    {
        base.Awake();

        transform = GetComponent<Transform>();
    }

    private void Update()
    {
        GetInput();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void GetInput()
    {
        moveDirection = Input.GetAxis("Horizontal");
    }

    private void Move()
    {
        previousVelocity = rBody.velocity.x;

        if(collisionBehaviour.onGround)
        {
            rBody.velocity = Vector3.Lerp(rBody.velocity, new Vector3(moveDirection * moveSpeed, 0) + new Vector3(0, rBody.velocity.y), accelerationSpeed * Time.deltaTime);
        }
        else if(Mathf.Abs(moveDirection) > 0.01f)
        {
            rBody.velocity = Vector3.Lerp(rBody.velocity, new Vector3(moveDirection * moveSpeed, 0) + new Vector3(0, rBody.velocity.y), accelerationSpeed * Time.deltaTime);
        }

        Anima.SetFloat("VelocityX", Mathf.Abs(rBody.velocity.x));
        player.transform.localScale = (Mathf.Abs(rBody.velocity.x) > 0.01f) ? new Vector3(1, 1, 1) : new Vector3(1, 1, -1);
    }
}
