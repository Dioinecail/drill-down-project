﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractBehaviour : MonoBehaviour
{
    // referenses
    public AbstractBehaviour[] behaviours;

    // cache
    protected Rigidbody2D rBody;
    protected CollisionBehaviour collisionBehaviour;
    protected Animator Anima;



    protected virtual void Awake()
    {
        rBody = GetComponent<Rigidbody2D>();
        collisionBehaviour = GetComponent<CollisionBehaviour>();
        Anima = GetComponentInChildren<Animator>();
    }

    public void DisableBehaviours()
    {
        foreach (AbstractBehaviour behaviour in behaviours)
        {
            behaviour.enabled = false;  
        }
    }

    public void EnableBehaviours()
    {
        foreach (AbstractBehaviour behaviour in behaviours)
        {
            behaviour.enabled = true;
        }
    }
}
