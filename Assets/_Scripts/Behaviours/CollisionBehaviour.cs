﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBehaviour : MonoBehaviour
{
    // referenses

    // paremeters
    public Vector2 StickToWallOffset = new Vector2(0, 0.65f);
    public float handRadius = 0.25f;

    public Vector2 feetOffset = new Vector2(0, 0.06f);
    public float FeetRadius = 0.18f;

    public bool onGround;
    public bool onWall;
    public bool wallOnRightSide;

    public LayerMask GroundMask;

    // cache
    protected Animator Anima;
    private Collider2D[] groundBuffer = new Collider2D[4];
    private Collider2D[] wallBuffer = new Collider2D[4];



    protected virtual void Awake()
    {
        Anima = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        ClearBuffers();

        Physics2D.OverlapCircleNonAlloc((Vector2)transform.position + feetOffset, FeetRadius, groundBuffer, GroundMask);
        Physics2D.OverlapCircleNonAlloc((Vector2)transform.position + StickToWallOffset, handRadius, wallBuffer, GroundMask);

        onWall = wallBuffer[0] != null;

        if(onWall)
        {
            wallOnRightSide = (wallBuffer[0].transform.position - transform.position).x > 0;
        }

        onGround = groundBuffer[0] != null;

        Anima.SetBool("OnGround", onGround);
        Anima.SetBool("OnWall", onWall && !onGround);
    }

    private void ClearBuffers()
    {
        for (int i = 0; i < groundBuffer.Length; i++)
        {
            groundBuffer[i] = null;
        }

        for (int i = 0; i < wallBuffer.Length; i++)
        {
            wallBuffer[i] = null;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere((Vector2)transform.position + feetOffset, FeetRadius);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere((Vector2)transform.position + StickToWallOffset, handRadius);
    }
}
