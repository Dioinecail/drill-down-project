﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShootBehaviour : AbstractBehaviour
{
    public UnityEvent onShoot;

    public float knockBackStrength = 5f;
    public float aimSpeed = 5f;
    public float shootCooldown = 0.1f;
    public float recoilArc = 5;
    public float shakeAmp;
    public float shakeDuration;

    public AmmoBehaviour projectilePrefab;
    public Transform gunArmTransform;
    public Transform gunTransform;
    public float shootOffset;

    private Coroutine shootCD_Coroutine;



    private void Update()
    {
        Aim();

        if(Input.GetButton("Fire1"))
        {
            OnShoot();
        }
    }

    public void OnShoot()
    {
        if (shootCD_Coroutine != null)
            return;

        // Spawn an ammo
        AmmoBehaviour projectile = GameobjectPoolSystem.Instantiate(projectilePrefab.gameObject, 
                                                                    gunTransform.position + gunTransform.right * shootOffset, 
                                                                    gunTransform.rotation)
                                                                    .GetComponent<AmmoBehaviour>();

        Vector3 targetDirection = Quaternion.Euler(0, 0, Random.Range(-recoilArc / 2, recoilArc / 2)) * gunTransform.right;
;
        projectile.SetDirection(targetDirection);

        // Knockback the player
        rBody.velocity += -(Vector2)gunArmTransform.right * knockBackStrength;
        onShoot?.Invoke();
        shootCD_Coroutine = StartCoroutine(ShootCooldown());
        CameraShake.Shake(shakeAmp, shakeDuration);
    }

    private void Aim()
    {
        // find the target position that the player is looking at
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector2 mousePosition = mouseRay.origin;
        Vector2 handDirection = mousePosition - (Vector2)gunArmTransform.position;

        Quaternion targetRotation = Quaternion.LookRotation(handDirection, Vector3.up);

        float angle = Vector2.SignedAngle(Vector2.right, handDirection);

        // rotate the gunArmTransform to that position
        gunArmTransform.rotation = Quaternion.Euler(0, 0, angle);
    }

    private IEnumerator ShootCooldown()
    {
        DisableBehaviours();
        yield return new WaitForSeconds(shootCooldown);
        EnableBehaviours();
        shootCD_Coroutine = null;
    }

    private void OnDrawGizmos()
    {
        if(gunTransform)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(gunTransform.position + gunTransform.right * shootOffset, .1f);
            Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 mousePosition = mouseRay.origin + mouseRay.direction * -Camera.main.transform.position.z;
            Vector3 handDirection = mousePosition - gunArmTransform.position;

            Gizmos.color = Color.cyan;

            Gizmos.DrawWireSphere(mousePosition, .1f);
            Gizmos.DrawRay(gunTransform.position, handDirection);

            Vector3 recoilArcBottom = Quaternion.Euler(0, 0, -recoilArc / 2) * gunTransform.right;
            Vector3 recoilArcTop = Quaternion.Euler(0, 0, recoilArc / 2) * gunTransform.right;

            Gizmos.color = Color.grey;
            Gizmos.DrawRay(gunTransform.position, recoilArcBottom);
            Gizmos.DrawRay(gunTransform.position, recoilArcTop);
        }
    }
}
