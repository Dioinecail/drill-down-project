﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBehaviour : AbstractBehaviour
{
    public static event Action onJump;

    public float JumpSpeed = 5.5f;



    private void Update()
    {
        if (Input.GetButtonDown("Jump") && collisionBehaviour.onGround)
        {
            OnJump();
        }

        Anima.SetFloat("VelocityY", rBody.velocity.y);
    }

    public void OnJump()
    {
        Anima.Play("Jump");
        rBody.isKinematic = false;
        rBody.velocity = new Vector3(rBody.velocity.x, JumpSpeed);
        onJump?.Invoke();
    }
}
