﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    // referenses
    public GameObject[] enemyPrefabs;

    // parameters
    public float spawnTimer;
    public Vector3[] spawnPoints;

    // cache
    private Coroutine coroutine_spawn;



    public void StartSpawning()
    {
        coroutine_spawn = StartCoroutine(SpawnCoroutine(spawnTimer));
    }

    public void StopSpawning()
    {
        if (coroutine_spawn != null)
            StopCoroutine(coroutine_spawn);
    }

    public void SpawnEnemy()
    {
        GameObject randomPick = RandomUtility.GetRandomObject(enemyPrefabs);
        Vector3 randomPosition = RandomUtility.GetRandomObject(spawnPoints);
        GameObject spawnedEnemy = GameobjectPoolSystem.Instantiate(randomPick, randomPosition, Quaternion.identity, true);
    }

    private IEnumerator SpawnCoroutine(float timer)
    {
        yield return new WaitForSeconds(timer);

        SpawnEnemy();

        yield return SpawnCoroutine(timer);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.gray;

        foreach (Vector3 s in spawnPoints)
        {
            Gizmos.DrawWireCube(s, Vector3.one);
        }
    }
}