﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    // parameters
    [Range(0, 1)]
    public float shakeLerp;

    // cache
    private static CameraShake Instance;
    private static new Transform transform;
    private Vector3 basePosition;
    private static Coroutine coroutine_shake;



    private void Awake()
    {
        transform = GetComponent<Transform>();
        basePosition = transform.position;
        Instance = this;
    }

    public static void Shake(float amplitude, float duration)
    {
        if (coroutine_shake != null)
            Instance.StopCoroutine(coroutine_shake);

        coroutine_shake = Instance.StartCoroutine(ShakeCoroutine(Instance.basePosition, amplitude, duration, Instance.shakeLerp));
    }

    private static IEnumerator ShakeCoroutine(Vector3 basePosition, float amplitude, float duration, float shakeLerp)
    {
        Vector3 shakeVector = Vector2.up;

        float timer = 0;

        while(timer < duration)
        {
            timer += Time.deltaTime;

            shakeVector = Vector2.up * Random.Range(0, amplitude);
            shakeVector = Quaternion.Euler(0, 0, Random.Range(0, 360)) * shakeVector;

            transform.position = Vector3.Lerp(transform.position, basePosition + shakeVector, shakeLerp);

            yield return null;
        }

        timer = 0;

        while(timer < 1)
        {
            timer += Time.deltaTime;

            transform.position = Vector3.Lerp(transform.position, basePosition, timer);

            yield return null;
        }

        transform.position = basePosition;
        coroutine_shake = null;
    }
}