﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviourBase : PoolableBase
{
    // parameters
    public float attackRadius;
    public float damage;
    public float attackCooldown;
    public LayerMask attackMask;
    public Vector3 damageOffset;

    // cache
    private Coroutine coroutine_attack;
    private Collider2D[] attackBuffer = new Collider2D[1];



    protected virtual void Update()
    {
        TryAttack();
    }

    private void TryAttack()
    {
        if (coroutine_attack != null)
            return;

        Attack();
    }

    protected virtual void Attack()
    {
        attackBuffer[0] = null;

        Physics2D.OverlapCircleNonAlloc(transform.position + damageOffset, attackRadius, attackBuffer, attackMask);

        if(attackBuffer[0] != null)
        {
            DamageableBase damageable = attackBuffer[0].GetComponent<DamageableBase>();

            if(damageable != null)
            {
                coroutine_attack = StartCoroutine(AttackCoroutine(attackCooldown));
                damageable.ReceiveDamage(damage);
            }
        }
    }

    private IEnumerator AttackCoroutine(float attackCooldown)
    {
        yield return new WaitForSeconds(attackCooldown);

        coroutine_attack = null;
    }

    private void OnDie()
    {
        ReturnToPool();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        GetComponent<DamageableBase>().onDie += OnDie;
    }

    private void OnDisable()
    {
        
        GetComponent<DamageableBase>().onDie -= OnDie;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + damageOffset, attackRadius);
    }
}
