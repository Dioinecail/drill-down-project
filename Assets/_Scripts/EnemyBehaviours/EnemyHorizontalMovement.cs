﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHorizontalMovement : MonoBehaviour
{
    private enum State
    {
        WALK,
        STOP
    }

    // parameters
    public float movementSpeed;
    public float searchRadius;
    public Vector3 searchOffset = new Vector3(0, 0.5f);

    public LayerMask searchMaskRotate;
    public LayerMask searchMaskDrill;

    // cache
    private State state = State.WALK;
    private Collider2D[] searchBufferDrill = new Collider2D[1];
    private RaycastHit2D[] searchBufferWall = new RaycastHit2D[1];
    private Vector2 currentDirection = Vector2.right;



    private void Update()
    {
        if(state == State.WALK)
        {
            Move();
            Search();
        }
    }

    private void Move()
    {
        transform.position += (Vector3)currentDirection * movementSpeed * Time.deltaTime;
    }

    private void Search()
    {
        searchBufferDrill[0] = null;
        searchBufferWall[0] = new RaycastHit2D();

        Physics2D.OverlapCircleNonAlloc(transform.position + searchOffset, searchRadius, searchBufferDrill, searchMaskDrill);
        Physics2D.RaycastNonAlloc(transform.position + searchOffset, currentDirection, searchBufferWall, searchRadius, searchMaskRotate);

        if(searchBufferDrill[0] != null)
        {
            state = State.STOP;
        }

        if(searchBufferWall[0].collider != null)
        {
            TurnAround();
        }
    }

    private void TurnAround()
    {
        currentDirection = -currentDirection;
    }

    private void OnEnable()
    {
        state = State.WALK;

        if (Random.Range(0, 100f) < 50)
            currentDirection = -currentDirection;
    }
}