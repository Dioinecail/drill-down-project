﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class DrillFuelManager : MonoBehaviour
{
    public static event Action<float> onFuelValueChanged;

    public float targetFuel;
    public float startingFuel;
    private float currentFuel;
    private float fuel
    {
        get { return currentFuel; }
        set
        {
            if (value >= targetFuel)
            {
                currentFuel = targetFuel;
            }
            else if (value <= 0)
            {
                currentFuel = 0;
            }
            else
                currentFuel = value;

            OnFuelValueChanged(currentFuel / targetFuel);
        }
    }

    public SpriteRenderer fuelRenderer;
    public SpriteRenderer fuelBackgroundRenderer;


    private void Awake()
    {
        fuel = startingFuel;
    }

    public void AddFuel(float amount)
    {
        fuel += amount;
    }

    public void RemoveFuel(float amount)
    {
        fuel -= amount;
    }

    private void OnFuelValueChanged(float percent)
    {
        fuelRenderer.size = new Vector2(fuelBackgroundRenderer.size.x, fuelBackgroundRenderer.size.y * percent);
        onFuelValueChanged?.Invoke(percent);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(DrillFuelManager))]
public class DrillFuelManagerEditor : Editor
{
    DrillFuelManager manager;

    private void OnEnable()
    {
        manager = (DrillFuelManager)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if(GUILayout.Button("Add 5 fuel"))
        {
            manager.AddFuel(5);
        }

        if (GUILayout.Button("Remove 5 fuel"))
        {
            manager.RemoveFuel(5);
        }
    }
}
#endif