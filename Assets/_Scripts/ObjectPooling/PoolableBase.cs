﻿using UnityEngine;

public abstract class PoolableBase : MonoBehaviour, IPoolableObject
{
    public float returnToPoolTimer = 5;

    [ContextMenu("Return to pool")]
    public virtual void ReturnToPool()
    {
        GameobjectPoolSystem.ReleaseObject(gameObject);
        CancelInvoke();
    }

    protected virtual void OnEnable()
    {
        if (returnToPoolTimer < 0)
            return;
        Invoke("ReturnToPool", returnToPoolTimer);
    }
}