﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrillDamageReceiver : DamageableBase
{
    public DrillFuelManager fuelManager;

    public override void ReceiveDamage(float damage)
    {
        fuelManager.RemoveFuel(damage);
    }
}