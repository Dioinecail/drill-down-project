﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    PLAYING,
    PAUSED,
    LEVEL_FAILED,
    LEVEL_FINISHED,
    LEVEL_TRANSITION,
    GAME_FINISHED
}

public class GameManager : MonoBehaviour
{
    private GameState state = GameState.PLAYING;
    public EnemyManager enemyManager;

    public bool spawnOnAwake;



    private void Awake()
    {
        if(spawnOnAwake)
            enemyManager.StartSpawning();
    }

    private void OnLevelFinish()
    {
        // allow level transition
    }

    private void OnLevelFailed()
    {
        // restart the level
    }

    public void LevelTransition()
    {
        if (state == GameState.LEVEL_TRANSITION)
            return;

        // transition to next level
    }

    private void OnFuelValueChanged(float percent)
    {
        switch (state)
        {
            case GameState.PLAYING:
                if (Mathf.Approximately(percent, 0))
                {
                    state = GameState.LEVEL_FAILED;
                    OnLevelFailed();
                }
                else if (Mathf.Approximately(percent, 1))
                {
                    state = GameState.LEVEL_FINISHED;
                    OnLevelFinish();
                }
                break;
            default:
                break;
        }
    }

    private void OnEnable()
    {
        DrillFuelManager.onFuelValueChanged += OnFuelValueChanged;
    }

    private void OnDisable()
    {
        DrillFuelManager.onFuelValueChanged -= OnFuelValueChanged;
    }
}