﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageableBase : MonoBehaviour
{
    public event Action onDie;
    // parameters
    public float maxHealth;

    // cache
    private float currentHealth;
    private float health
    {
        get { return currentHealth; }
        set
        {
            if (value >= maxHealth)
                currentHealth = maxHealth;
            else if (value <= 0)
            {
                currentHealth = 0;
                onDie?.Invoke();
            }
            else
                currentHealth = value;
        }
    }


    private void OnEnable()
    {
        health = maxHealth;
    }

    public virtual void ReceiveDamage(float damage)
    {
        health -= damage;
    }

    public virtual void AddHealth(float amount)
    {
        health += amount;
    }
}