﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomUtility
{
    public static T GetRandomObject<T>(T[] objects)
    {
        int rnd = Random.Range(0, objects.Length);

        return objects[rnd];
    }
}
